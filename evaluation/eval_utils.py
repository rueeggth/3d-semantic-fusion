import copy
import csv
import os

import numpy as np
import open3d as o3d
import trimesh


def load_ground_truth_mesh(path, mesh_name):
    # 1. Loads the groundtruth mesh
    mesh_path = os.path.join(path, mesh_name)
    mesh = o3d.io.read_triangle_mesh(mesh_path)

    # 2. Loads groundtruth class labels
    labels_path = os.path.join(path, 'vertice_labels.txt')
    labels = np.loadtxt(labels_path, dtype='i', delimiter=',')

    # 3. Loads groundtruth instance labels
    instance_path = os.path.join(path, 'pred_mask')
    instance_files = os.listdir(instance_path)
    instances = np.zeros(len(labels), dtype=int)
    n_instances_per_class = np.zeros(41)
    for i,file in enumerate(instance_files):
        file_path = os.path.join(instance_path, file)
        instance_txt = np.loadtxt(file_path).astype(bool)
        instance_classes = np.unique(labels[instance_txt])
        if len(instance_classes) != 1:
            print('Warning: Ground truth instance', file, 'belongs to multiple classes!')
        instance_class = instance_classes[0]
        n_instances_per_class[instance_class] += 1
        instances[instance_txt] = n_instances_per_class[instance_class]

    return mesh, labels, instances

def load_generated_mesh(path):
    mesh = o3d.io.read_triangle_mesh(path)
    labels = (np.asarray(mesh.vertex_colors)*255).astype(int)[:,0]
    instances = (np.asarray(mesh.vertex_colors)*255).astype(int)[:,1]

    return mesh, labels, instances


def get_class_dict(path):
    """
    Gets mapping class number <-> class name and a list of all class labels
    """

    with open(path, newline='') as csvfile:
        labelreader = csv.reader(csvfile, delimiter=';', quotechar='#')
        class_name_dict = {}
        for row in labelreader:
            class_name_dict[int(row[0])] = row[1]

    classes = np.fromiter(class_name_dict.keys(), dtype=int)
    return class_name_dict, classes

def get_masked_mesh(mesh, mask):
    """
    Returns a mesh containing only the vertices of mesh inside the mask.
    """
    output_mesh = copy.deepcopy(mesh)
    output_mesh.remove_vertices_by_mask(np.bitwise_not(mask))
    return output_mesh

def is_mesh_empty(mesh):
    """
    Checks whether mesh is empty. True if mesh contains no vertices or no
    triangular surfaces.
    """
    return mesh.is_empty() or np.asarray(mesh.triangles).shape[0] == 0

def voxelize_mesh(mesh, voxel_size):
    """
    Returns mesh converted into a voxel grid.

    mesh: open3d mesh
    return: trimesh voxel grid
    """

    vox = trimesh.Trimesh(
        vertices=np.asarray(mesh.vertices),
        faces=np.asarray(mesh.triangles),
        vertex_normals=np.asarray(mesh.vertex_normals),
        ).voxelized(voxel_size)

    return vox


def get_IoU(mesh1, mesh2, voxel_size, discard_outliers=False):
    """
    Calculates the IoU score between two meshes on a resolution of voxel_size.
    """
    if is_mesh_empty(mesh1) or is_mesh_empty(mesh2):
        return 0

    vox1 = voxelize_mesh(mesh1, voxel_size)
    vox2 = voxelize_mesh(mesh2, voxel_size)

    vox1_matrix, vox2_matrix, origin, end = expand_matrices(vox1, vox2, voxel_size)

    if discard_outliers:
        vox1_matrix, vox2_matrix = remove_outliers(vox1_matrix, vox2_matrix)

    intersect = np.sum(np.bitwise_and(vox1_matrix, vox2_matrix))
    union     = np.sum(np.bitwise_or (vox1_matrix, vox2_matrix))
    IoU = intersect/union

    return IoU

def get_mask_IoU(mesh1, mesh2, mask1, mask2, voxel_size):
    """
    Calculates the IoU score between two meshes with the given masks on a
    resolution of voxel_size.
    """
    masked_mesh1 = get_masked_mesh(mesh1, mask1)
    masked_mesh2 = get_masked_mesh(mesh2, mask2)

    IoU = get_IoU(masked_mesh1, masked_mesh2, voxel_size, discard_outliers=False)

    return IoU


def expand_matrices(vox_1, vox_2, voxel_size):
    # vox_1 = gt_vox.copy()
    # vox_2 = recon_vox.copy()
    matrix_1 = vox_1.matrix
    matrix_2 = vox_2.matrix

    start_1 = np.array(vox_1.translation)
    start_2 = np.array(vox_2.translation)

    end_1 = start_1 + np.array(matrix_1.shape) * voxel_size
    end_2 = start_2 + np.array(matrix_2.shape) * voxel_size

    start = np.array([min(start_1[0], start_2[0]),
                      min(start_1[1], start_2[1]),
                      min(start_1[2], start_2[2])])

    end   = np.array([max(end_1[0], end_2[0]),
                      max(end_1[1], end_2[1]),
                      max(end_1[2], end_2[2])])

    dimensions = ((end-start)/voxel_size + 1).astype(int)

    out_mat_1 = np.zeros(dimensions).astype(bool)
    out_mat_2 = np.zeros(dimensions).astype(bool)

    start_index_1 = ((start_1 - start)/voxel_size).astype(int)
    start_index_2 = ((start_2 - start)/voxel_size).astype(int)

    end_index_1 = (start_index_1 + np.array(matrix_1.shape)).astype(int)
    end_index_2 = (start_index_2 + np.array(matrix_2.shape)).astype(int)

    out_mat_1[start_index_1[0]:end_index_1[0],
              start_index_1[1]:end_index_1[1],
              start_index_1[2]:end_index_1[2]] = matrix_1

    out_mat_2[start_index_2[0]:end_index_2[0],
              start_index_2[1]:end_index_2[1],
              start_index_2[2]:end_index_2[2]] = matrix_2

    return out_mat_1, out_mat_2, start, end

def visualise_vox_matrices(gt_vox_matrix, recon_vox_matrix):
    """
    This function visualises the two occupancy matrices. The occupancy is
    encoded in the color. If a voxel is occupied by both occupancy matrices, it
    is colored red. If it is only occupied in recon_vox_matrix the voxel is
    colored blue, if it is only occupied in gt_vox_matrix, it is green.
    """
    encoding = trimesh.voxel.encoding.DenseEncoding(np.bitwise_or (gt_vox_matrix, recon_vox_matrix))
    vox = trimesh.voxel.VoxelGrid(encoding)

    colors = np.zeros((gt_vox_matrix.shape[0], gt_vox_matrix.shape[1], gt_vox_matrix.shape[2], 4))
    colors[gt_vox_matrix,:] = [0, 255, 0, 100]
    colors[recon_vox_matrix,:] = [0, 0, 255, 100]
    colors[np.bitwise_and(gt_vox_matrix, recon_vox_matrix),:] = [255, 0, 0, 255]

    vox.as_boxes(colors).show()


def remove_outliers(gt_vox_matrix, recon_vox_matrix):
    """
    Removes all points outside the ground truth occupancy matrix as they are
    outliers. This function also crops both matrices in order to remove the
    empty voxels outside the room in both matrices.
    """
    gt_voxel_indices = np.where(gt_vox_matrix)
    gt_x_min = np.min(gt_voxel_indices[0])
    gt_y_min = np.min(gt_voxel_indices[1])
    gt_z_min = np.min(gt_voxel_indices[2])
    gt_x_max = np.max(gt_voxel_indices[0])
    gt_y_max = np.max(gt_voxel_indices[1])
    gt_z_max = np.max(gt_voxel_indices[2])
    gt_vox_matrix = gt_vox_matrix[gt_x_min:gt_x_max, gt_y_min:gt_y_max, gt_z_min:gt_z_max]
    recon_vox_matrix = recon_vox_matrix[gt_x_min:gt_x_max, gt_y_min:gt_y_max, gt_z_min:gt_z_max]

    return gt_vox_matrix, recon_vox_matrix


def save_to_csv(classes, class_name_dict, class_scores, instance_scores, overall_IoU, recon_path, use_gt_ply):
    """
    Saves evaluation results to csv file.
    """
    if use_gt_ply:
        output_path = recon_path.split('.')[0] + "-vs-gt_ply-score.csv"
    else:
        output_path = recon_path.split('.')[0] + "-vs-gt_masks-score.csv"
    print("Saved scores to: ", output_path)

    with open(output_path, "w") as output_file:
        output_file.write('Class label;Class IoU;Classwise Instance Average Precision;' + '\n')
        for i_class, class_ in enumerate(classes):
            output_file.write('{};{};{};'.format(class_name_dict[class_], class_scores[i_class], instance_scores[i_class]) + '\n')
        output_file.write('mean;{};{};'.format(np.nanmean(class_scores), np.nanmean(instance_scores)) + '\n')
        output_file.write('Overall Reconstruction IoU;{};;'.format(overall_IoU) + '\n')
