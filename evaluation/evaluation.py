import eval_utils
import numpy as np

# Grid size for voxelising the mesh.
voxel_size = 0.10


# If set to True, reconstruction is evaluated against ground truth ply. If set
# to False, it is evaluated against another reconstructed ply, e.g. the
# reconstruction of the ground truth masks.
use_gt_ply = False

if use_gt_ply:
    # Path to the folder containing all ground truth information (a ply mesh
    # specified below, a "vertice_labels.txt" containing the vertex class labels,
    # and a folder "pred_mask" containing the individual instances.).
    gt_path = '/media/nico_schulthess/Elements/3DVision/ScanNet/scans/scene0217_00/'
    # Name of the ground truth ply mesh located at gt_path.
    gt_mesh_name = 'scene0217_00_vh_clean_2.labels.ply'
else:
    # Path to the ply mesh reconstructed from ground truth masks.
    gt_path = 'gt_masks_evaluation.ply'

# Path to the reconstructed ply mesh.
recon_path = 'rcnn_every_frame.ply'




def main():
    # Loads the ground truth mesh.
    if use_gt_ply:
        gt_mesh, gt_labels, gt_instances = eval_utils.load_ground_truth_mesh(gt_path, gt_mesh_name)
    else:
        gt_mesh, gt_labels, gt_instances = eval_utils.load_generated_mesh(gt_path)
    print('Ground truth loaded')

    # Loads the reconstructed mesh
    recon_mesh, recon_labels, recon_instances = eval_utils.load_generated_mesh(recon_path)
    print('Reconstruction loaded')

    class_name_dict, eval_classes = eval_utils.get_class_dict('Dataset/labels_40_evaluation.csv')

    # Place holders for inserting the scores. Both scores are calculated per class.
    # The class score is the classwise IoU, the instance score is the classwise
    # average precision (AP) as in MS COCO.
    class_scores = np.ones(len(eval_classes)) * np.nan
    instance_scores = np.ones(len(eval_classes)) * np.nan

    for i_class, class_ in enumerate(eval_classes):

        # Part 1: Get the class IoU score.

        # Create the class meshes.
        gt_class_mesh = eval_utils.get_masked_mesh(gt_mesh, gt_labels == class_)
        recon_class_mesh = eval_utils.get_masked_mesh(recon_mesh, recon_labels == class_)

        # Abort if class mesh is empty.
        if (eval_utils.is_mesh_empty(gt_class_mesh) or eval_utils.is_mesh_empty(recon_class_mesh)):
            class_scores[i_class] = np.nan
            continue

        # Calculate the class IoU score.
        class_scores[i_class] = eval_utils.get_IoU(gt_class_mesh, recon_class_mesh, voxel_size, discard_outliers=False)

        # Part 2: Get the instance score.

        # Gets the available instances for this class in ground truth and
        # reconstruction.
        gt_inst = np.unique(gt_instances[gt_labels == class_])
        recon_inst = np.unique(recon_instances[recon_labels == class_])

        # Calculates the IoU scores for every instance combination and saves it to
        # IoU_values.
        IoU_values = np.zeros((len(gt_inst), len(recon_inst)))
        for i,i_gt in enumerate(gt_inst):
            for j,i_recon in enumerate(recon_inst):
                gt_mask = np.bitwise_and(gt_labels == class_, gt_instances == i_gt)
                recon_mask = np.bitwise_and(recon_labels == class_, recon_instances == i_recon)
                IoU_values[i,j] = eval_utils.get_mask_IoU(gt_mesh, recon_mesh, gt_mask, recon_mask, voxel_size)

        precision = []
        recall = []

        # Calculates precision and recall for different IoU thresholds.
        for IoU_level in np.arange(0.3, 1, 0.01):
            # Place holder for the instances in the reconstruction matching an
            # instance in the ground truth. If an instance matches, the corresponding
            # element in instance_matches is set to 1, otherwise it is set to 0.
            instance_matches = np.zeros((len(gt_inst), len(recon_inst)), dtype=int)

            # Removes scores below the IoU threshold.
            instance_IoU = np.copy(IoU_values)
            instance_IoU[instance_IoU < IoU_level] = 0

            # Repeats until there are no matching instances left.
            while (instance_IoU != 0).any():
                # Finds instance combination with highest IoU and extracts its indices.
                i_gt, i_recon = np.where(instance_IoU == np.max(instance_IoU))
                i_gt = i_gt[0]
                i_recon = i_recon[0]

                # Removes combinations with the same ground truth instance or with
                # the same reconstruction instance.
                instance_IoU[i_gt, :] = 0
                instance_IoU[:, i_recon] = 0
                # Marks the matching combination.
                instance_matches[i_gt, i_recon] = 1

            # Based on the matches, calculates true positives, false negatives and
            # false positives. Then calculates precision and recall values.
            TP = np.sum(np.sum(instance_matches, axis=1) == 1)
            FN = np.sum(np.sum(instance_matches, axis=1) == 0)
            FP = np.sum(np.sum(instance_matches, axis=0) == 0)
            precision.append(TP / (TP + FP))
            recall.append(TP / (TP + FN))

        # Calculates the average precision as the area under the recall (x-axis) -
        # precision (y-axis) curve.
        instance_scores[i_class] = np.sum([(recall[i] - recall[i+1]) * (precision[i] + precision[i+1]) * 0.5 for i in range(len(precision)-1)])

    # Calculates the overall reconstruction IoU.
    overall_IoU = eval_utils.get_IoU(gt_mesh, recon_mesh, voxel_size, discard_outliers=True)



    # Output of the resulting scores.
    eval_utils.save_to_csv(eval_classes, class_name_dict, class_scores, instance_scores, overall_IoU, recon_path, use_gt_ply)

if __name__ == '__main__':
    main()
